package com.epam.task06.lambdas.streams.controller;

import com.epam.task06.lambdas.streams.model.task3.DataGenerator;


abstract class Task3Controller extends Executor {

  DataGenerator dataGenerator;

  Task3Controller() {
    dataGenerator = new DataGenerator();
  }

  abstract Integer getMax();

  abstract Integer getMin();

  abstract Double getAvg();

  abstract int getSum();

  abstract int getSumWithReduce();

  abstract int getNumberIntegersBiggerAvg();
}