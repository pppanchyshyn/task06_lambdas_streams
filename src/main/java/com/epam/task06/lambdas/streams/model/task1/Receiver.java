package com.epam.task06.lambdas.streams.model.task1;

public interface Receiver {

  Number accept(int value1, int value2, int value3);
}
