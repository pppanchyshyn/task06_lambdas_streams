package com.epam.task06.lambdas.streams.controller;

import java.util.Collection;

public interface Executable {

  Collection<String> execute();
}
