package com.epam.task06.lambdas.streams.controller;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import com.sun.deploy.util.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task4Controller extends Executor {

  public Task4Controller() {
  }

  private long getUniqueWordsNumber(List<String> words) {
    return words.
        stream().
        distinct().
        count();
  }

  private List<String> getUniqueSortedWords(List<String> words) {
    return words.
        stream().
        distinct().
        sorted(String::compareToIgnoreCase).
        collect(Collectors.toList());
  }

  private Map<String, Long> getWordsFrequency(List<String> words) {
    return words.
        stream().
        collect(groupingBy(Function.identity(), counting()));
  }

  private List<Character> getCharList(List<String> words) {
    List<Character> characters = new ArrayList<>();
    String stringFromList = StringUtils.join(words, "");
    for (char c : stringFromList.toCharArray()
        ) {
      characters.add(c);
    }
    return characters;
  }

  private Map<Character, Long> getLettersFrequency(List<Character> characters) {
    return characters.
        stream().
        filter(Character::isLowerCase).
        collect(groupingBy(Function.identity(), counting()));
  }

  @Override
  public Collection<String> execute() {
    List<String> words = dataReader.enterWords();
    results.add(String.format("Your words: %s", Arrays.toString(words.toArray())));
    results.add(String.format("Number of unique words: %d", getUniqueWordsNumber(words)));
    results.add(
        String.format("Sorted list of all unique words: %s",
            Arrays.toString(getUniqueSortedWords(words).toArray())));
    results.add("Occurrence number of each word in the text: ");
    for (String name : getWordsFrequency(words).keySet()
        ) {
      String value = getWordsFrequency(words).get(name).toString();
      results.add(String.format("%s repeats %s times", name, value));
    }
    results.add("Occurrence number of each symbol except upper case characters: ");
    for (Character name : getLettersFrequency(getCharList(words)).keySet()
        ) {
      String value = getLettersFrequency(getCharList(words)).get(name).toString();
      results.add(String.format("%s repeats %s times", name, value));
    }
    return results;
  }
}
