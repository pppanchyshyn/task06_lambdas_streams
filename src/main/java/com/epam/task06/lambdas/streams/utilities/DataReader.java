package com.epam.task06.lambdas.streams.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataReader {

  private Logger logger = LogManager.getLogger(DataReader.class);
  private Scanner input = new Scanner(System.in);

  public DataReader() {
  }

  public int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  public int enterMenuItem() {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem <= Constants.EXIT_MENU_ITEM)) {
        break;
      } else {
        logger.info("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

  public List<String> enterWords() {
    logger.info("Enter words separated by pressing Enter. To stop enter blank line: ");
    String word;
    List<String> words = new ArrayList<>();
    Scanner input = new Scanner(System.in);
    while (!(word = input.nextLine()).isEmpty()) {
      words.add(word);
    }
    return words;
  }
  public String enterFilmName() {
    logger.info("Enter name of film: ");
    return input.nextLine();
  }
}
