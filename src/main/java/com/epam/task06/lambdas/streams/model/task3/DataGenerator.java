package com.epam.task06.lambdas.streams.model.task3;

import com.epam.task06.lambdas.streams.utilities.Constants;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataGenerator {

  private List<Integer> randomList;
  private Integer[] randomArray;

  public DataGenerator() {
    randomList = Stream
        .generate(() -> (int) (Constants.TASK3_RANDOM_COEFFICIENT * Math.random()))
        .limit(Constants.TASK3_CONTAINERS_SIZE)
        .collect(Collectors.toList());
    randomArray = Stream
        .generate(() -> (int) (Constants.TASK3_RANDOM_COEFFICIENT * Math.random()))
        .limit(Constants.TASK3_CONTAINERS_SIZE)
        .toArray(Integer[]::new);
  }

  public List<Integer> getRandomList() {
    return randomList;
  }

  public Integer[] getRandomArray() {
    return randomArray;
  }
}
