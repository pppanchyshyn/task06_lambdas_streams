package com.epam.task06.lambdas.streams.controller;

import com.epam.task06.lambdas.streams.model.task1.Statistic;
import java.util.Collection;

public class Task1Controller extends Executor {

  public Task1Controller() {
  }

  @Override
  public Collection<String> execute() {
    logger.info("Processing task 1. Add three integers");
    results.clear();
    int value1 = dataReader.enterInteger();
    int value2 = dataReader.enterInteger();
    int value3 = dataReader.enterInteger();
    Number max = Statistic.maxValue.accept(value1, value2, value3);
    Number avg = Statistic.average.accept(value1, value2, value3);
    results.add("Max value - " + max);
    results.add("Average - " + avg);
    return results;
  }
}
