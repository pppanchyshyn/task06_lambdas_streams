package com.epam.task06.lambdas.streams.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Task3ControllerList extends Task3Controller{
private List<Integer> list;
public Task3ControllerList(){
  list = dataGenerator.getRandomList();
}
  Integer getMax() {
    return list
        .stream()
        .max(Integer::compareTo)
        .get();
  }

  Integer getMin() {
    return list
        .stream()
        .min(Integer::compareTo)
        .get();
  }

  Double getAvg() {
    return list
        .stream()
        .mapToInt(Integer::intValue)
        .average()
        .getAsDouble();
  }

  int getSum() {
    return list
        .stream()
        .mapToInt(Integer::intValue)
        .sum();
  }

  int getSumWithReduce() {
    return list
        .stream()
        .mapToInt(Integer::intValue)
        .reduce((acc, x) -> acc + x)
        .getAsInt();
  }

  int getNumberIntegersBiggerAvg() {
    return (int) list
        .stream()
        .mapToInt(Integer::intValue)
        .filter(x -> x > getAvg())
        .count();
  }
  public Collection<String> execute() {
    logger.info("Testing task 3 for lists");
    results.clear();
    results.add(Arrays.toString(list.toArray()));
    results.add("Max value - " + getMax());
    results.add("Min value - " + getMin());
    results.add("Average - " + getAvg());
    results.add("Number of values that are bigger than average - " + getNumberIntegersBiggerAvg());
    results.add("Sum - " + getSum());
    results.add("Sum using reduce method - " + getSumWithReduce());
    return  results;
  }
}
