package com.epam.task06.lambdas.streams.utilities;

public class Constants {

  public final static int EXIT_MENU_ITEM = 4;
  public final static int TASK3_CONTAINERS_SIZE = 10;
  public final static int TASK3_RANDOM_COEFFICIENT = 100;

  private Constants() {
  }
}
