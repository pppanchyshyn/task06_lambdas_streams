package com.epam.task06.lambdas.streams.view;

import com.epam.task06.lambdas.streams.controller.Executable;
import com.epam.task06.lambdas.streams.controller.Task1Controller;
import com.epam.task06.lambdas.streams.controller.Task3ControllerArray;
import com.epam.task06.lambdas.streams.controller.Task3ControllerList;
import com.epam.task06.lambdas.streams.controller.Task4Controller;
import com.epam.task06.lambdas.streams.utilities.Constants;
import com.epam.task06.lambdas.streams.utilities.DataReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements Runnable {

  private Logger logger = LogManager.getLogger(ConsoleView.class);
  private Map<Integer, Executable> map;
  private DataReader dataReader;

  ConsoleView() {
    dataReader = new DataReader();
    map = new HashMap<>();
    map.put(0, new Task1Controller());
    map.put(1, new Task3ControllerList());
    map.put(2, new Task3ControllerArray());
    map.put(3, new Task4Controller());
  }

  private void print(Collection<String> collection) {
    for (String message : collection
        ) {
      logger.info(message);
    }
  }

  private void printMenu() {
    logger.info("To print results of task 1 press 0\n"
        + "To print results of task 3 for list press 1\n"
        + "To print results of task 3 for array press 2\n"
        + "To print results of task 4 press 3\n"
        + "To exit program press 4\n"
        + "\nMake your choice: ");
  }

  @Override
  public void run() {
    int menuItem;
    while (true) {
      printMenu();
      menuItem = dataReader.enterMenuItem();
      if (menuItem == Constants.EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      logger.info("-------------------------------------------------------------------------");
      print(map.get(menuItem).execute());
      logger.info("-------------------------------------------------------------------------");
    }
  }
}

