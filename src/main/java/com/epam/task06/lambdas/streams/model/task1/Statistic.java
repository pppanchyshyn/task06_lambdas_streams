package com.epam.task06.lambdas.streams.model.task1;

import java.util.stream.IntStream;

public class Statistic {

  public static Receiver maxValue = (value1, value2, value3) ->
      IntStream.of(value1, value2, value3)
          .max()
          .getAsInt();

  public static Receiver average = (value1, value2, value3) ->
      IntStream.of(value1, value2, value3)
          .average()
          .getAsDouble();
}
