package com.epam.task06.lambdas.streams.controller;

import java.util.Arrays;
import java.util.Collection;

public class Task3ControllerArray extends Task3Controller {

  private Integer[] array;

  public Task3ControllerArray() {
    array = dataGenerator.getRandomArray();
  }

  Integer getMax() {
    return Arrays
        .stream(array)
        .max(Integer::compareTo)
        .get();
  }

  Integer getMin() {
    return Arrays
        .stream(array)
        .min(Integer::compareTo)
        .get();
  }

  Double getAvg() {
    return Arrays
        .stream(array)
        .mapToInt(Integer::intValue)
        .average()
        .getAsDouble();
  }

  int getSum() {
    return Arrays
        .stream(array)
        .mapToInt(Integer::intValue)
        .sum();
  }

  int getSumWithReduce() {
    return Arrays
        .stream(array)
        .mapToInt(Integer::intValue)
        .reduce((acc, x) -> acc + x)
        .getAsInt();
  }

  int getNumberIntegersBiggerAvg() {
    return (int) Arrays
        .stream(array)
        .mapToInt(Integer::intValue)
        .filter(x -> x > getAvg())
        .count();
  }

  public Collection<String> execute() {
    logger.info("Testing task 3 for arrays");
    results.clear();
    results.add(Arrays.toString(array));
    results.add("Max value - " + getMax());
    results.add("Min value - " + getMin());
    results.add("Average - " + getAvg());
    results.add("Number of values that are bigger than average - " + getNumberIntegersBiggerAvg());
    results.add("Sum - " + getSum());
    results.add("Sum using reduce method - " + getSumWithReduce());
    return results;
  }
}
