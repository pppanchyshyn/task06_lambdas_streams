package com.epam.task06.lambdas.streams.view;

public class Application {

  public static void main(String[] args) {
    new ConsoleView().run();
  }
}
