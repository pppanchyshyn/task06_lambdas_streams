package com.epam.task06.lambdas.streams.controller;

import com.epam.task06.lambdas.streams.utilities.DataReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class Executor implements Executable {

  Logger logger = LogManager.getLogger(Executor.class);
  List<String> results;
  DataReader dataReader;

  Executor() {
    results = new ArrayList<>();
    dataReader = new DataReader();
  }
}
